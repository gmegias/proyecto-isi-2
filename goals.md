#Goals

##G.1 Contexto y objetivo general
Las residencias necesitan modernizarse, y contar con un sistema de comunicación entre enfermeros y pacientes que mejore el confort y la seguridad de estos. También queremos registrar de manera automática lo que sucede en la residencia, así como controlar las ubicaciones de los pacientes. 
Aumentará la capacidad y la eficiencia, mejorando la organización dentro de la residencia.

##G.2 Situación actual
- Los pacientes de la residencia no están monitorizados.
- Las enfermeras no tienen claro a qué pacientes deben atender en cada momento, careciendo de una organización adecuada.
- No queda registrado todo lo que ocurre en la residencia.
- No queda claro el orden de atención a los avisos según su nivel de urgencia.

##G.3 Beneficios esperados
- Poder analizar los datos generados por el sistema.
- Ordenar por prioridad las alertas generadas.
- Mejorar la organización de los enfermeros.
- No perder ninguna petición de ayuda.

##G.4 Funcionalidad
El sistema debe monitorizar a los pacientes de la residencia, así como su ubicación. En caso de que el terminal de un paciente envíe una alerta, esta debe ser procesada en un servidor, quedar registrada, y generar la respuesta adecuada para que el paciente sea atendido.

##G.5 Casos de uso alto-nivel
###Caso general

Ocurre un evento
Se genera una alerta en el terminal del paciente
Los terminales cercanos reenvían los mensajes hasta que llega al servidor de la sala
El servidor de la sala lo envía al servidor central
El servidor central decide la forma de actuación y envía la respuesta al servidor de la sala correspondiente.
El servidor de la sala envía el mensaje y llega al enfermero, quien decide atender al paciente.
El paciente es atendido.
Se registra el evento en la base de datos.

##G.6 Limitaciones
El sistema no garantiza que un mensaje generado por un microbit llegue al servidor de la sala, en caso de que no tenga suficiente cobertura.
No se contemplará la situación de la ausencia permanente de enfermeros en la residencia.
No se contemplan fallos en los sensores, así como en el resto de equipos.

##G.7 Stakeholders
Los directores de la residencia definen la funcionalidad del sistema junto con el equipo de trabajo.
Los pacientes y los enfermeros hacen uso del sistema.



