#Environment

##E.1 Glosario
-API REST: Interfaz de aplicación segura basada en arquitectura cliente/servidor con características concretas (sin estado, etc.).
-Micro:bit: micro-ordenador que hará de terminal portátil en el sistema.
-Flask: framework minimalista para desarrollar APIs rápidamente
-QoS: Quality of Service. Mide distintos parámetros en un sistema de comunicaciones que determinan la calidad del mismo, 
como el retardo de la comunicación, la tasa de errores, etc.
-AP: Punto de acceso.
-SQLite: lenguaje de programación de bases de datos relacionales.


##E.2 Componentes
- API REST para manejar los datos producidos por el sistema de monitorización

##E.3 Constraints
- Terminales pequeños para los pacientes y enfermeros para su confort.
- El terminal necesita disponer de: acelerómetro para detectar caídas, botones para realizar solicitudes, radio para enviar mensajes y un display para
poder mostrarlos.
- El sistema debe ser capaz de reconocer la ubicación de los terminales dentro del espacio de la residencia.
- El sistema debe tener alcance para que los terminales puedan tener cobertura en todo el espacio de la residencia.
- Los terminales deben poder intercambiar mensajes y mostrarlos en los receptores.
- El sistema debe poder registrar todos los mensajes enviados.


##E.4 Assumptions
- No es necesario distinguir ubicaciones dentro de una misma sala.
- Ya contamos con instalación eléctrica e Internet en la residencia.
- No se requieren QoS.
- Los pacientes y enfermeros llevarán asignado un terminal permanentemente.
- Siempre habrá algún enfermero en la residencia.
- Los mensajes siempre llegarán a su destinatario.

##E.5 Efectos
- Monitorización a los pacientes
- Forma de atención a los pacientes
- Organización de los enfermeros de la residencia


##E.6 Invariantes
- Cuidado rutinario de los pacientes
- El paciente debe obtener atención cuando lo solicite
- Horario de actividades en la residencia

















