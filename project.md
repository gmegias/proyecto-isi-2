#Project

##P.1 Roles y personal
- Fernando Real Rojas: Maintainer
- Bianca Radu: Maintainer
- Guillermo Megías Areas: Project owner

##P.2 Imposiciones técnicas
- Uso de microbits como terminales, programados con Mu editor.
- Empleo del lenguaje Python para programar.
- Uso de GitLab para gestionar y organizar el proyecto.
- Uso de Flask para la API REST.
- Uso de SQLite3 como lenguaje de programación para la base de datos.

##P.3 Programación de tareas

##P.4 Tareas y deadlines

##P.5 Elementos requeridos
- Ordenadores para poder programar los terminales y la API Rest.
- Servidor para gestionar los mensajes y la base de datos.
- Editor de textos para programar en Python.
- Conexión a internet.
- GitLab.

##P.6 Potenciales problemas
- Caídas del servidor.
- Fallos en los terminales.
- Insuficiente cobertura.


##P.7 Proceso de requerimientos
- Comenzamos captando los requisitos de funcionalidad.
- Traducimos los requerimientos de funcionalidad a requerimientos técnicos.
- Organizamos los grupos del proyecto para abordar las diferentes tareas, definiendo deadlines.
- Integramos el software desarrollado por cada grupo.
- Pruebas y testing del sistema completo.
- Revisión de requisitos y satisfación del cliente.




