#System book

##S.1 Componentes
- Micro:bit programado en python
- API Rest con Flask programada en Python
- Base de datos SQLite
- Front-End de la API con HTML y CSS para el gestor del sistema en la residencia.
- Front-End en el propio microbit para los pacientes y enfermeros.

##S.2 Funcionalidad
Requisitos funcionales:
-El sistema debe monitorizar a los pacientes mediante el uso de terminales, conociendo la sala en la que están.
-Los terminales deben detectar caídas de los pacientes y enviar alertas a los enfermeros.
-Los terminales deben enviar avisos cuando el paciente pulse el botón correspondiente.
- El sistema debe registrar todos los mensajes enviados, así como emisor y destinatario, y la fecha y hora.
- Los terminales deben reenviar los mensajes que reciban para ampliar el área de cobertura.
- Deberá haber un servidor por sala que envíe los mensajes al servidor central.

Requisitos no funcionales:
- Uso de microbits
- Relay de microbits
- Sqlite3
- APIs REST (toda la comunicación que no sea de los microbits será con http y API REST) y usaremos también Flask
- Tolerancia a fallos (alarmas si ocurren)
-Testing
-CI/CD
-Base Station por USB

##S.3 Interfaces
- El microbit estará configurado para pacientes y enfermeros para facilitar su uso, mediante la programación de botones para hacer posible su uso. También contará con un display para mostrar los mensajes.
- Se utilizará una API Rest que procesará los mensajes recibidos de los microbits en función del tipo que sean, así como almacenar/editar estos y otros datos en una base de datos con SQLite3.


##S.4 Casos de uso

### Case 1
Name: Enfermero no atiende a la ayuda
Scope: Ayuda de operaciones en residencia
Primary actor: Enfermero
Preconditions: Anciano necesita ayuda
Trigger: Enfermero recibe mensaje de ayuda
Main scenario:
1. Enfermero ha recibido un mensaje de ayuda
2. El enfermero está ocupado, no puede ayudar al anciano
3. El enfermero responde al mensaje diciendo que no puede ayudar
4. El mensaje se reenvía a otro enfermero para que atienda al paciente
5. El segundo enfermero atiende al anciano
Success guarantee: El anciano recibe ayuda
Extensions:
1. El enfermero, en lugar de denegar la ayuda, la deriva a un compañero
2. El enfermero, en lugar de denegar la ayuda, la pospone (si el grado de urgencia lo permite)
Stakeholders and interests: Pacientes, familiares, residencia, empresa, sociedad...

### Case 2
Name: Enfermero no disponible
Scope: Ayuda de operaciones en residencia
Primary actor: Enfermero
Preconditions: Enfermero no está disponible
Trigger: Enfermero recibe mensaje de ayuda
Main scenario:
1. El enfermero termina de atender a un paciente en una sala
2. El enfermero necesita ir al baño, y no podrá atender a ancianos en ese tiempo
3. El enfermero envía un mensaje anunciando su ausencia.
4. El sistema tiene en cuenta que el enfermero no está disponible
5. El mensaje se reenvía a otro enfermero para que atienda al paciente
6. El segundo enfermero atiende al anciano
Success guarantee: El anciano recibe ayuda
Stakeholders and interests: Pacientes, familiares, residencia, empresa, sociedad...

### Case 3
Name: Anciano pide ayuda
Scope: Ayuda de operaciones en residencia
Primary actor: Anciano
Context of use:
Preconditions: Anciano necesita ayuda
Trigger: Anciano pide ayuda
Main scenario:
1. El anciano necesita ayuda por cualquier motivo
2. El anciano pulsa el botón de ayuda no urgente
3. El sistema recibe el mensaje
4. Como es mensaje no urgente no lo envía, hay un mensaje prioritario
5. El anciano recibe un mensaje de espera
Success guarantee: El anciano es atendido más tarde
Extensions:
1. El anciano envía un mensaje un urgente por lo que se prioriza y es atendido antes
Stakeholders and interests: Pacientes, familiares, residencia, empresa, sociedad...

### Case 4
Name: Servido controla mensajes
Scope: Ayuda de gestión de mensajes en residencia
Primary actor: Servidor
Preconditions: Anciano envía mensaje urgente, anciano 2 envía mensaje no urgente y enfermero envía mensaje de ausencia
Trigger: El servidor recibe mensajes
Main scenario:
1. El servidor recibe los mensajes de todas las bases
2. El servidor analiza los tipos de mensajes y los clasifica por prioridad
3. El servidor anula los enfermeros ausentes y analiza los disponibles en cada zona
4. El servios envía los mensajes urgentes
5. El servios envía los mensajes no urgentes
Success guarantee: Todos los mensajes se envían en el orden correcto
Stakeholders and interests: Pacientes, familiares, residencia, empresa, sociedad...

### Case 5
Name: Envío del mensaje
Scope: Ayuda de gestión de mensajes en residencia
Primary actor: Anciano
Preconditions: Anciano necesita ayuda
Main scenario:
1. El anciano necesita ayuda
2. El anciano pulsa el botón
3. La base-station recibe el mensaje
4. La base-station envía el mensaje al servidor
5. El servidor reenvía el mensaje al enfermero
6. La base-station destino recibe el mensaje
7. La base-station reenvía el mensaje al enfermero 
Success guarantee: Todos los mensajes se envían y reciben
Extensions:
1. El enfermero envía un mensaje y lo recibe un anciano (por ejemplo, hora de comer)
Stakeholders and interests: Pacientes, familiares, residencia, empresa, sociedad...

##Historias de usuario

### Historia 1
Como un anciano
Para que yo reciba ayuda
Quiero poder recibir ayuda

### Historia 2
Como un enfermero
Para que acepte mensajes
Quiero poder recibir mensajes y aceptarlos

### Historia 3
Como un servidor
Para que yo gestione los mensajes
Quiero conocer la ubicación de todos los enfermeros y ancianos

### Historia 4
Como un base-station
Para que yo reciba los mensajes
Quiero que los ancianos reenvíen los mensajes

### Historia 5
Como un anciano
Para que yo no muera
Quiero que el servidor envíe los mensajes de las pastillas

##S.5 Prioridades
1- El sistema debe ser capaz de detectar caídas y alertas para avisar a los enfermeros, retransmitiendo el mensaje si es necesario.
2- Ubicación de los pacientes en las salas
3- Avisos rutinarios
4- Aceptar/Denegar/Posponer/Delegar alertas en caso de los enfermeros.
5- Distinguir niveles de alerta
6- No recibir mensajes no prioritarios si estoy ocupado.


##S.6 Criterios de verificación y aceptación
-Si se detectan caídas o un paciente pulsa un botón, el microbit envía una alerta.
- Si al menos un enfermero recibe cada mensaje, no perderemos ninguna alerta.
-Si un paciente envía una alerta en una sala, un enfermero de otra sala debe recibir el mensaje si en la del paciente no hay.
- Las alertas prioritarias se procesan antes que las no prioritarias.
- Detectar efectivamente la ubicación del paciente.
- Enfermero tiene opciones de actuación ante una alerta, y en caso de denegación llega a otro enfermero.